from steganoPy import SteganoPy
import argparse

parser = argparse.ArgumentParser(description='Steganography a secret black/white image in a colored image.')

parser.add_argument('--hide', help='Path for secret black/white image.')
parser.add_argument('--in-image', help='Path for original colored image to hide secret in it.')
parser.add_argument('--output', help='Path for the output image.')
parser.add_argument('--extract', help='Path for ccolored image with secret.')

parser.add_argument('--show', dest='show', action='store_true')
parser.add_argument('--no-show', dest='show', action='store_false')
parser.set_defaults(show=False)

args = parser.parse_args()

steganoPy = SteganoPy()

if args.hide and args.in_image and args.output:
    steganoPy.hide_secret(args.hide).in_image(args.in_image).output(args.output, args.show)
elif args.extract and args.output:
    steganoPy.extract(args.extract).to(args.output, args.show)
else:
    parser.print_help()
