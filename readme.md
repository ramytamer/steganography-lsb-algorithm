Simple bash script to run the script along folder of images to extract their secret images:

```
#!bash
for file in test-cases/*.png; do python main.py --extract ${file} --output ${file}_secret.png --show; done
```