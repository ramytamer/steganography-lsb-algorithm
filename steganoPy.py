from PIL import Image
import sys, os

class SteganoPy():
    
    def __init__(self):
        pass

    def hide_secret(self, secret_image_path):
        try:
            self.secret_image = Image.open(secret_image_path)
            return self
        except IOError as e:
            # raise e
            print 'Secret message file not found!'
            sys.exit(0)

    def in_image(self, original_image):
        try:
            self.original_image = Image.open(original_image)
            return self
        except IOError as e:
            # raise e
            print 'Original message file not found!'
            sys.exit(0)


    def output(self, output_path, show):
        secret_image_pixels   = self.secret_image.getdata()
        original_image_pixels = self.original_image.getdata()

        output_image_data = []

        for i, secret_pixel in enumerate(secret_image_pixels):
            original_pixel = original_image_pixels[i]

            if secret_pixel[0] == 0:
                new_value = ( original_pixel[0]+1, original_pixel[1], original_pixel[2] ) if (original_pixel[0] % 2 == 0 ) else original_pixel
            else:
                new_value = ( original_pixel[0]-1, original_pixel[1], original_pixel[2] ) if (original_pixel[0] % 2 == 1 ) else original_pixel
                
            output_image_data.append( new_value )


        self.original_image.putdata(output_image_data)

        try:
            os.remove(output_path)
        except Exception as e:
            pass
        finally:
            self.original_image.save(output_path, 'PNG')
            if show:
                self.original_image.show()
                

    def extract(self, message):
        try:
            self.extract_image = Image.open(message)
            return self
        except IOError as e:
            # raise e
            print 'File not found!'

    def to(self, output_path, show=False):
        extract_pixels = self.extract_image.getdata() 

        original_image_data = []
        for pixel in extract_pixels:
            new_value = (255, 255, 255) if (pixel[0] % 2 == 0) else (0, 0, 0)
            original_image_data.append( new_value )

        self.extract_image.putdata(original_image_data)

        try:
            os.remove(output_path)
        except Exception as e:
            pass
        finally:
            self.extract_image.save(output_path, 'PNG')
            if show:
                self.extract_image.show()
                